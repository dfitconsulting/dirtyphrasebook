# Monty Python's Hungarian Phrase Book Translator

This application provides translations, via REST web service, for the common phrases used in the skit.
Please take no offense to the phrases or translations, they are just provided for fun. I take no
responsibility for the actual translations to the alternate languages.

# Build the Applications

Gradle handles building the applications, providing an executable jar file containing a Spring Boot
application per sub project.  It contains an embedded Tomcat, fully production ready to be deployed 
in multiple cloud environments

* ./gradlew clean build

Build without executing all tests:

* ./gradlew clean assemble

Target just one application:

* ./gradlew :discovery:clean :discovery:build

# Application Startup

The start up order is important only for 2 of the components in the demo. In this configuration, the 
Discovery service is required to be first, followed by the Config server. This is provide the applications
the ability to discover the location of the Config server and bootstrap the applications accordingly.

## Spring Boot Dashboard

* discovery : http://localhost:8761
* config : http://localhost:8888
* service : http://localhost:8090
* api : http://localhost:9000
* zuul : http://localhost:8765
* hystrixdashboard : http://localhost:7979
* turbine : http://localhost:8989

## Command Line

* java -jar discovery/build/libs/discovery-0.0.1.SNAPSHOT.jar : http://localhost:8761
* java -jar config/build/libs/config-0.0.1.SNAPSHOT.jar : http://localhost:8888
* java -jar service/build/libs/service-0.0.1.SNAPSHOT.jar : http://localhost:8090
* java -jar api/build/libs/api-0.0.1.SNAPSHOT.jar : http://localhost:9000
* java -jar zuul/build/libs/zuul-0.0.1.SNAPSHOT.jar : http://localhost:8765
* java -jar hystrixdashboard/build/libs/hystrixdashboard-0.0.1.SNAPSHOT.jar : http://localhost:7979
* java -jar turbine/build/libs/turbine-0.0.1.SNAPSHOT.jar : http://localhost:8989

# Docker Commands

## Build Docker Images

* ./gradlew :discovery:clean :discovery:build :discovery:buildDocker
* ./gradlew :config:clean :config:build :config:buildDocker
* ./gradlew :service:clean :service:build :service:buildDocker
* ./gradlew :api:clean :api:build :api:buildDocker
* ./gradlew :zuul:clean :zuul:build :zuul:buildDocker
* ./gradlew :turbine:clean :turbine:build :turbine:buildDocker
* ./gradlew :hystrixdashboard:clean :hystrixdashboard:build :hystrixdashboard:buildDocker

## Common Commands

* docker images - lists all images
* docker rm - removes one or more containers
* docker rmi - removes one or more images
* docker ps - lists containers
* docker logs

## Run Docker Containers

* docker run -m 512M -d --hostname discovery --name discovery -p 8761:8761 mpdpb/discovery
* docker run -m 512M -d --hostname config --name config -p 8888:8888 --link discovery:discovery mpdpb/config
* docker run -m 512M -d --link discovery:discovery mpdpb/service
* docker run -m 512M -d --hostname api --name api -p 9000:9000 --link discovery:discovery mpdpb/api
* docker run -m 512M -d --hostname zuul --name zuul -p 8765:8765 --link discovery:discovery mpdpb/zuul
* docker run -m 512M -d --hostname turbine --name turbine -p 8989:8989 --link discovery:discovery mpdpb/turbine
* docker run -m 512M -d --hostname hystrixdashboard --name hystrixdashboard -p 7979:7979 --link discovery:discovery mpdpb/hystrixdashboard

# Cloud Foundry

Cloud Foundry is a PaaS environment that supports deploying Spring Boot applications, as well as traditional War deployment. It also supports other languages like groovy, ruby, python, etc.

This application contains a script for deploying the complete application to https://run.pivotal.io, Pivotal's hosted version of Cloud Foundry.

## Requirements

* Cloud Foundry command line client installed
* Account on https://run.pivotal.io (or any installation of Cloud Foundry)

## Build and Deploy to Cloud Foundry

* ./cf.sh

This script will execute ./gradlew clean assemble to build all the applications, connect to https://run.pivotal.io, delete all apps, services and routes, then deploy all apps and services.


