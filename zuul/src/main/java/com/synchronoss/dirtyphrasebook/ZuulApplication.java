package com.synchronoss.dirtyphrasebook;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.PercentageBasedSampler;
import org.springframework.cloud.sleuth.sampler.SamplerProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableDiscoveryClient
@EnableZuulProxy
public class ZuulApplication {

	public static void main( String[] args ) {
		
		new SpringApplicationBuilder( ZuulApplication.class ).web( true ).run( args );
	
	}
	
	@Bean
	public Sampler defaultSampler() {
	
		return new PercentageBasedSampler( new SamplerProperties() );
	}
	
}
