$(function(){

   // jQuery methods go here...
   populateDropdownWithLanguages();

   $( '#translateForm' ).on( 'submit', function( e ) {
      e.preventDefault();
      console.log( 'requesting translation' );

      translate();

   });

});

function populateDropdownWithLanguages() {

    $.ajax({
        type: 'GET',
        url: '/api/translator',
        success: function( data, status ) {
            console.dir( status );
            console.dir( data );

            var languageSelect = $( '#language' );
            $.each( data, function( key, value ) {
                console.debug( key );

                var option = $( '<option />' );
                option.val( key );
                option.text( key );
                if( value == 'en' ) {
                  option.attr( 'selected', 'selected' );
                }
                languageSelect.append( option );

            });
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log( textStatus );
            console.log( errorThrown );
        }

    });

}

function translate() {

    $.ajax({
        type: 'POST',
        url: '/api/translator',
        data: $( '#translateForm' ).serialize(),
        success: function( data, status ) {
            console.dir( status );
            console.dir( data );

            $( '#translation' ).empty();

            var p = $( '<p/>' );
            p.text( data.to );
            $( '#translation' ).append( p );

 //           translations();

        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log( textStatus );
            console.log( errorThrown );

            $( '#translation' ).empty();

            var p = $( '<p/>' );
            p.text( 'ERROR' );
            $( '#translation' ).append( p );

        }

    });
}

function translations() {

    $.ajax({
        type: 'GET',
        url: '/api/translator/translations',
        success: function( data, status ) {
            console.dir( status );
            console.dir( data );

            var header = $( '<header/>' );
            header.text( 'Available Translations' );
            $( '#translation' ).append( header );

            var ul = $( '<ul/>' );
            $.each( data, function( i ) {

                var li = $( '<li/>' );
                li.text( data[i] );
                ul.append( li );

            });
            $( '#translation' ).append( ul );

        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            console.log( textStatus );
            console.log( errorThrown );

        }

    });
}
