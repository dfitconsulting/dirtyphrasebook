#!/bin/bash
set -e

#
# the big CloudFoundry installer
#

CLOUD_DOMAIN=${DOMAIN:-run.pivotal.io}
CLOUD_TARGET=api.${DOMAIN}

function login(){
  cf api | grep ${CLOUD_TARGET} || cf api ${CLOUD_TARGET} --skip-ssl-validation
  cf a | grep OK || cf login -s dirtyphrasebook
}

function app_domain(){
  D=`cf apps | grep $1 | tr -s ' ' | cut -d' ' -f 6 | cut -d, -f1`
  echo $D
}

function deploy_app(){

  APP_NAME=$1
  cd $1
  cf push $APP_NAME  --no-start -b https://github.com/cloudfoundry/java-buildpack.git
  APPLICATION_DOMAIN=`app_domain $APP_NAME`
  echo determined that application_domain for $APP_NAME is $APPLICATION_DOMAIN.
  cf env $APP_NAME | grep APPLICATION_DOMAIN || cf set-env $APP_NAME APPLICATION_DOMAIN $APPLICATION_DOMAIN
  cf set-health-check $APP_NAME none
  cf restart $APP_NAME
  cd ..
}

function deploy_service(){
  N=$1
  D=`app_domain $N`
  JSON='{"uri":"http://'$D'"}'
  echo cf cups $N  -p $JSON
  cf cups $N -p $JSON
}

function deploy_discovery() {
  NAME=discovery
  deploy_app $NAME
  deploy_service $NAME
}

function deploy_config() {
  NAME=config
  deploy_app $NAME
}

function deploy_api() {
  NAME=api
  deploy_app $NAME
}

function deploy_hystrix_dashboard() {
  NAME=hystrixdashboard
  deploy_app $NAME
}

function deploy_turbine() {
  NAME=turbine
  deploy_app $NAME
}

function deploy_zuul() {
  NAME=zuul
  deploy_app $NAME
}

function deploy_dirtyphrasebook_service() {
  NAME=service
  deploy_app $NAME
}

function reset(){
  cf d turbine
  cf d hystrixdashboard
  cf d service
  cf d zuul
  cf d api
  cf d config
  cf d discovery
  cf ds discovery
  cf delete-orphaned-routes
}

./gradlew clean assemble

login
reset
deploy_discovery
deploy_config
deploy_zuul
deploy_api
deploy_hystrix_dashboard
deploy_turbine
deploy_dirtyphrasebook_service
