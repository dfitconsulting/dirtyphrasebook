/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author dmfrey
 *
 */
@RunWith( SpringJUnit4ClassRunner.class )
@SpringApplicationConfiguration( classes = DirtyPhraseBookApplication.class )
public class TranslationServiceTest {

    private static final String TEST_LANGUAGE = "English";

    @Autowired
    TranslationService translator;

    @Before
    public void setup() {
    	
        translator.setLanguage( TEST_LANGUAGE );

    }

    @Test
    public void testUniformTranslation() {
        
    	String translated = translator.getTranslation( "Hello World!" );
    	
        assertThat( "Lowercase matches", translator.getTranslation( "hello world!" ), equalTo( translated ) );
        assertThat( "Without punctuation matches", translator.getTranslation( "hello world" ), equalTo( translated ) );
        assertThat( "Multiple spaces matches", translator.getTranslation( "  hello       world   " ), equalTo( translated ) );
    
    }
    
}
