/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

/**
 * @author dmfrey
 *
 */
@RunWith( JUnit4.class )
public class StringSanitizerTest {

    public static final String EXPECTED_OUTPUT = "hello world";

    @Test
    public void testNormaliseCase() {
    	
        assertThat( "All capitals", StringSanitizer.normalizeCase( "HELLO WORLD" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "All lowercase", StringSanitizer.normalizeCase( "hello world" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "Camelcase", StringSanitizer.normalizeCase( "Hello World" ), equalTo( EXPECTED_OUTPUT ) );
    
    }

    @Test
    public void testRemoveNonLetters() {
    
    	assertThat( "Exclamation", StringSanitizer.removeNonLetters( "hello world!" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "Quotes", StringSanitizer.removeNonLetters( "\"hello world\"" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "Greek", StringSanitizer.removeNonLetters( "ελληνικά" ), equalTo( "ελληνικά" ) );

    }

    @Test
    public void testRemoveDiacritics() {
        
    	assertThat( "A ring", StringSanitizer.removeDiacritics( "hållo world" ), equalTo( "hallo world" ) );
        assertThat( "E acute", StringSanitizer.removeDiacritics( "héllo world" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "E grave", StringSanitizer.removeDiacritics( "hèllo world" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "O dieresis", StringSanitizer.removeDiacritics( "hello wörld" ), equalTo( EXPECTED_OUTPUT ) );
    
    }

    @Test
    public void testRemoveSpaces() {
    
    	assertThat( "Leading space", StringSanitizer.sanitize( " hello world" ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "Trailing space", StringSanitizer.sanitize( "hello world " ), equalTo( EXPECTED_OUTPUT ) );
        assertThat( "Double space", StringSanitizer.removeMultipleSpaces("hello  world"), equalTo( EXPECTED_OUTPUT ) );
    
    }

    @Test
    public void testSanitise() {
    	
        assertThat( "Capitals, accents, and punctuation", StringSanitizer.sanitize( " Héllo      Wörld ! " ), equalTo( EXPECTED_OUTPUT ) );
    
    }
    
}
