/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

/**
 * @author dmfrey
 *
 */
public class DeveloperException extends RuntimeException {
	
    public DeveloperException( String s ) {
        super( s );
    
    }
    
}
