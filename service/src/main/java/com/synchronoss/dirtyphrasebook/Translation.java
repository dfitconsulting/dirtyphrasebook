/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;

import lombok.Data;

/**
 * @author dmfrey
 *
 */
@Data
public class Translation {

//	@JsonFormat( pattern = "yyyy-MM-dd" )
	private final LocalDateTime startTime;
	private final OffsetDateTime offsetStartTime;
	private final String from;
	private final String to;
	
}
