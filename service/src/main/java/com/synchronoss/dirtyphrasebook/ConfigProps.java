package com.synchronoss.dirtyphrasebook;

import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.validation.Valid;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author dmfrey
 *
 */
@Configuration
@ConfigurationProperties( prefix = "configuration" )
@RefreshScope
@Data
@Slf4j
public class ConfigProps {

	private LocalDateTime startTime;
	private OffsetDateTime offsetStartTime;
	private String[] languages;
	private String[] readableLanguages;
	
	@Valid
	private Map<String, List<String>> phrases = new HashMap<>();
	
	@PostConstruct
	public void afterPropertiesSet() {
		
		log.info( "afterPropertiesSet : languages=" + languages );
		log.info( "afterPropertiesSet : readableLanguages=" + readableLanguages );
		log.info( "afterPropertiesSet : phrases=" + phrases );

	}
	
}
