/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import java.time.OffsetDateTime;
import java.time.format.DateTimeParseException;

import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;

/**
 * @author dmfrey
 *
 */
@Configuration
public class OffsetDateTimeConverterConfiguration {

	@Bean
	@ConfigurationPropertiesBinding
	public Converter<String, OffsetDateTime> offsetDateTimeConverter() {
		
		return new OffsetDateTimeConverter();
	}
	
	public static class OffsetDateTimeConverter implements Converter<String, OffsetDateTime> {

		@Override
		public OffsetDateTime convert( String source ) {
			
			try {
				
				return OffsetDateTime.parse( source );
				
			} catch( DateTimeParseException e ) {
			
				throw new IllegalArgumentException( String.format( "Could not convert '%s' into an OffsetDateTime", source ), e ); 
			}
			
		}
		
	}
	
}
