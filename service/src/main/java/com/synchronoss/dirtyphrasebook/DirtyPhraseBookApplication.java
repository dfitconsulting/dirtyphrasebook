package com.synchronoss.dirtyphrasebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.PercentageBasedSampler;
import org.springframework.cloud.sleuth.sampler.SamplerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.core.convert.ConversionService;
import org.springframework.format.support.DefaultFormattingConversionService;
import org.springframework.format.support.FormattingConversionService;

import com.synchronoss.dirtyphrasebook.OffsetDateTimeConverterConfiguration.OffsetDateTimeConverter;

@SpringBootApplication
@EnableDiscoveryClient
public class DirtyPhraseBookApplication {

	public static void main( String[] args ) {
		
		SpringApplication.run( DirtyPhraseBookApplication.class, args );
	
	}

	@Bean
	public Sampler defaultSampler() {
	
		return new PercentageBasedSampler( new SamplerProperties() );
	}

	@Bean
	public static ConversionService conversionService() {
	 
		FormattingConversionService conversionService = new DefaultFormattingConversionService();
		conversionService.addConverter( new OffsetDateTimeConverter() );
		
		return conversionService;
	}

}
