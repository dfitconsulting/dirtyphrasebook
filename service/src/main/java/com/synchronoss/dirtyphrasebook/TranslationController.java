/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * @author dmfrey
 *
 */
@RestController
@RequestMapping( "/" )
@Slf4j
public class TranslationController {

	@Autowired
	TranslationService service;
	
	@Autowired
	ConfigProps configProps;
	
	@RequestMapping( method = RequestMethod.POST )
	public @ResponseBody Translation translate( @RequestParam String from, @RequestParam( value = "language", defaultValue = "English", required = false ) String language ) {
		log.debug( "translate : enter" );
		
		service.setLanguage( language );
		
		String to = service.getTranslation( from );
		
		Translation translation = new Translation( configProps.getStartTime(), configProps.getOffsetStartTime(), from, to );
		log.debug( "translate : translation=" + translation );
		
		log.debug( "translate : exit" );
		return translation;
	}
	
	@RequestMapping( method = RequestMethod.GET )
	public @ResponseBody Map<String, String> supportedLanguages() {
		log.debug( "supportedLanguages : enter" );
		
		log.debug( "supportedLanguages : exit" );
		return service.getSupportedLanguages();
	}
	
	@RequestMapping( value = "/translations", method = RequestMethod.GET )
	public @ResponseBody List<String> translations( @RequestParam( value = "language", defaultValue = "English", required = false ) String language ) {
		log.debug( "translations : enter, language=" + language );

		service.setLanguage( language );

		log.debug( "translations : exit" );
		return service.getTranslations();
	}

}
