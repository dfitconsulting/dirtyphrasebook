/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * @author dmfrey
 *
 */
@Service
@Slf4j
@RefreshScope
public class TranslationService {

    private static final Pattern LANGUAGE_COUNTRY_PATTERN = Pattern.compile("(.*?)_(.*?)");

    private Map<String, String> languagesMap = new HashMap<>();
    private Map<Integer, Integer> hashLookup = new HashMap<>();

    private String[] translations;
    private Locale defaultLocale = Locale.getDefault();

    @Autowired
    ConfigProps props;
    
	@PostConstruct
    public void afterPropertiesSet() {
        
        if( props.getLanguages().length != props.getReadableLanguages().length ) {
            
        	throw new DeveloperException( "languages and readableLanguages need to be the same size. Bad developer!" );
        }
        
        languagesMap = new HashMap<>( props.getReadableLanguages().length );
        for( int index = 0; index < props.getReadableLanguages().length; index++ ) {
            
        	languagesMap.put( props.getReadableLanguages()[ index ], props.getLanguages()[ index ] );
        
        }
        
        updateHashLookup();
      
        log.info( "afterPropertiesSet : props=" + props );
    }

    public void setLanguage( String language ) {
        log.info( "setLanguage : enter, language=" + language );
        
    	translations = getTranslations( languagesMap.get( language ) );
    
    }

    public String getTranslation( String source ) {
        log.info( "getTranslation : enter, source=" + source );
       
    	String sanitized = StringSanitizer.sanitize( source );
        int hashcode = sanitized.hashCode();
        Integer index = hashLookup.get( hashcode );
        if( index == null ) {
        	
            index = Math.abs( hashcode % translations.length );
        
        }
        
        return translations[ index ];
    }

    public List<String> getTranslations() {
        log.info( "getTranslations : enter" );

    	return Arrays.asList( translations );
    }
     
    public Map<String, String> getSupportedLanguages() {
        log.info( "getSupportedLanguages : enter" );

    	return languagesMap;
    }
    
    private String[] getTranslations( String language ) {
        log.info( "getTranslations : enter, language=" + language );
        
        return props.getPhrases().get( language ).toArray( new String[ props.getPhrases().get( language ).size() ] );
    }

    private void updateHashLookup() {
        
    	Collection<String> languages = languagesMap.values();
        
    	hashLookup.clear();
        
        for( String language : languages ) {
        
        	String[] allTranslations = getTranslations( language );
            for( int index = 0; index < allTranslations.length; index++ ) {
                
            	String sanitized = StringSanitizer.sanitize( allTranslations[ index ] );
                int sanitizedHashCode = sanitized.hashCode();
                if( hashLookup.get( sanitizedHashCode ) != null ) {
                  
                	throw new DeveloperException( "Dude, we have duplicate hash codes for translations strings" );
                }
            
                hashLookup.put( sanitizedHashCode, index );
        
            }
        
        }
        
    }

    public Locale getLocaleForLanguage( String readableLanguage ) {
        
    	String language = languagesMap.get( readableLanguage );
        String country = null;
        
        Matcher matcher = LANGUAGE_COUNTRY_PATTERN.matcher(language);
        if( matcher.matches() ) {

        	language = matcher.group( 1 );
            country = matcher.group( 2 );
        
        }
        
        if( language.equals( defaultLocale.getLanguage() ) ) {
            
        	return defaultLocale;
        }
    
        return StringUtils.isEmpty( country ) ? new Locale( language ) : new Locale( language, country );
    }
    
}
