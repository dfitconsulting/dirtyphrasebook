/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.sleuth.zipkin.stream.EnableZipkinStreamServer;

/**
 * @author dmfrey
 *
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableZipkinStreamServer
public class ZipkinApplication {

	public static void main( String[] args ) {
		
		SpringApplication.run( ZipkinApplication.class, args );
	
	}
	
}
