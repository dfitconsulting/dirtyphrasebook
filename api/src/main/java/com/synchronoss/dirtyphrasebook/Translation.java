/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

/**
 * @author dmfrey
 *
 */
public class Translation {

	private String from;
	private String to;
	
	/**
	 * 
	 */
	public Translation() { }
	
	/**
	 * @param from
	 * @param to
	 */
	public Translation( String from, String to ) {
		
		this.from = from;
		this.to = to;
	
	}

	public String getFrom() {
	
		return from;
	}

	public void setFrom( String from ) {
	
		this.from = from;
	
	}

	/**
	 * @return the to
	 */
	public String getTo() {
	
		return to;
	}

	/**
	 * @param to the to to set
	 */
	public void setTo( String to ) {
		
		this.to = to;
	
	}

	
}
