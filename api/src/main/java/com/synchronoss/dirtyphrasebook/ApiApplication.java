package com.synchronoss.dirtyphrasebook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.sleuth.Sampler;
import org.springframework.cloud.sleuth.sampler.PercentageBasedSampler;
import org.springframework.cloud.sleuth.sampler.SamplerProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableDiscoveryClient
@EnableCircuitBreaker
public class ApiApplication {

	public static void main( String[] args ) {
		
		SpringApplication.run( ApiApplication.class, args );
	
	}
	
	@Bean
	public RestTemplate restTemplate() {
		
		return new RestTemplate();
	}

	@Bean
	public Sampler defaultSampler() {
	
		return new PercentageBasedSampler( new SamplerProperties() );
	}
	
}
