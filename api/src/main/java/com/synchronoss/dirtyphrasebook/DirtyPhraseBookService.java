/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.loadbalancer.LoadBalancerClient;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import lombok.extern.slf4j.Slf4j;

/**
 * @author dmfrey
 *
 */
@Service
@Slf4j
public class DirtyPhraseBookService {

	@Autowired
    private LoadBalancerClient loadBalancer;
	
	@Autowired
	RestTemplate restTemplate;
	
	@HystrixCommand( fallbackMethod = "defaultLanguages" )
	public Map<String, String> supportedLanguages() {
		log.info( "supportedLanguages : enter" );

		ServiceInstance instance = loadBalancer.choose( "service" );
        URI serviceUri = URI.create(String.format("http://%s:%s", instance.getHost(), instance.getPort()));
        
		return restTemplate.getForEntity( serviceUri, Map.class ).getBody();
	}

	public Map<String, String> defaultLanguages() {
		log.info( "defaultLanguages : enter" );
		
		Map<String, String> languages = new HashMap<>();
    	languages.put( "English", "en" );
    	
    	return languages;
	}

	@HystrixCommand( fallbackMethod = "defaultTranslate" )
	public Translation translate( String from, String language ) {
		log.info( "translate : enter - from=" + from + ", language=" + language );

		ServiceInstance instance = loadBalancer.choose( "service" );
        URI serviceUri = URI.create(String.format("http://%s:%s", instance.getHost(), instance.getPort()));

        MultiValueMap<String, Object> parts = new LinkedMultiValueMap<String, Object>();
		parts.add( "from", from );
		parts.add( "language", language );
		
		return restTemplate.postForEntity( serviceUri, parts, Translation.class ).getBody();
	}

	public Translation defaultTranslate( String from, String language ) {
		log.info( "defaultTranslate : enter - from=" + from + ", language=" + language );
	
		return new Translation( from, "I don't like spam!" );
	}

	@HystrixCommand( fallbackMethod = "defaultTranslations" )
	public List<String> translations( String language ) {
		log.info( "translations : enter - language=" + language );

		ServiceInstance instance = loadBalancer.choose( "service" );
        URI serviceUri = URI.create(String.format("http://%s:%s/translations?language=%s", instance.getHost(), instance.getPort(), language ));

        return restTemplate.getForEntity( serviceUri, List.class ).getBody();
	}

	public List<String> defaultTranslations( String language ) {
		log.info( "defaultTranslations : enter - language=" + language );
	
		List<String> translations = new ArrayList<>();
		translations.add( "I don't like spam!" );
		
		return translations;
	}

}
