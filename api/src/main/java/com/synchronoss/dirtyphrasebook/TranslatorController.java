/**
 * 
 */
package com.synchronoss.dirtyphrasebook;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

/**
 * @author dmfrey
 *
 */
@RestController
@RequestMapping( "/translator" )
@Slf4j
public class TranslatorController {

	@Autowired
	DirtyPhraseBookService service;
	
	@RequestMapping( method = RequestMethod.GET )
	public @ResponseBody Map<String, String> supportedLanguages() {
		log.info( "supportedLanguages : enter" );
		
		return service.supportedLanguages();
	}

	@RequestMapping( method = RequestMethod.POST )
	public @ResponseBody Translation translate( @RequestParam String from, @RequestParam( value = "language", defaultValue = "English", required = false ) String language ) {
		log.info( "translate : enter - from=" + from + ", language=" + language );

		return service.translate( from, language );
	}
	
	@RequestMapping( value = "/translations" )
	public @ResponseBody List<String> translations( @RequestParam( value = "language", defaultValue = "English", required = false ) String language ) {
		log.info( "translations : enter - language=" + language );

		return service.translations( language );
	}
	
}
